import org.junit.Test;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class BucketSortTests {

	@Test
	public void casoTeste1() {
		int[] testArray = new int[]{-1, 0, 1, 2, 3};

		BucketSort.bucketSort(testArray);

		assertArrayEquals(new int[]{-1, 0, 1, 2, 3}, testArray);
	}

	@Test
	public void casoTeste2() {
		int[] testArray = new int[]{3, 2, 1, 0, -1};

		BucketSort.bucketSort(testArray);

		assertArrayEquals(new int[]{-1, 0, 1, 2, 3}, testArray);
	}

	@Test
	public void casoTeste3() {
		int[] testArray = new int[]{1, 1, 1, 1};

		BucketSort.bucketSort(testArray);

		assertArrayEquals(new int[]{1, 1, 1, 1}, testArray);
	}

	@Test
	public void casoTeste4() {
		int[] testArray = new int[]{0, 1.1, 2, 3};

		BucketSort.bucketSort(testArray);

		assertArrayEquals(new int[]{-1, 0, 1, 2, 3}, testArray);
	}

	@Test
	public void casoTeste5() {
		int[] testArray = new int[]{0};

		BucketSort.bucketSort(testArray);

		assertArrayEquals(new int[]{0}, testArray);
	}

	@Test
	public void casoTeste6() {
		int[] testArray = null;

		NullPointerException thrown = assertThrows(NullPointerException.class, () -> BucketSort.bucketSort(testArray));

		assertNull(testArray);
	}
}
